package com.example;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorArchivos implements Runnable {

    String filename; 
    Listado listado;
    
    public ServidorArchivos(Listado listado) {
        this.listado = listado;
    }
    @Override
    public void run() {

        ServerSocket ss;
        try {
            //TODO implementar logs
            ss = new ServerSocket(9002);
            System.out.println("ActualizadorListado: Estoy escuchado en 9002");

        while (true){
            Socket client = ss.accept();
            
            System.out.println("ServidorArchivos: Atendiendo al cliente: "+client.getPort());

            HiloServidorArchivos sh = new HiloServidorArchivos(client, listado);
            // 2do paso
            Thread serverThread = new Thread(sh);
            // 3er paso
            serverThread.start();

        }
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }                
    }

}
