package com.example;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.net.*;
import java.io.*;
import com.example.Peer;
import com.google.gson.Gson;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

class Client {

    private String address;
    // Peer peer;

    public Client(Peer peer) {
        try {
            ServerSocket peerAsServer = new ServerSocket(peer.getPort());

            // 1. pongo a escuchar el peer
            // 2. creo un hilo menu cliente
            ClientMenu menuCliente = new ClientMenu(peer);// El peer que envio soy yo + dentro de menuCliente debo
                                                          // establecer la conexion con el siguiente peer
            Thread menuThread = new Thread(menuCliente);
            menuThread.start();

            // Pongo a escuchar el peer
            while (true) {
                Socket socket = peerAsServer.accept();
                ClientHilo clientHilo = new ClientHilo(socket, peer);
                Thread clientThread = new Thread(clientHilo);
                clientThread.start();
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println(e.getMessage());
        }
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static void main(String[] args) {
        //Levanto la aplicacion e indico en que puerto escucho a los demas peers y el path de los archivos a compartir
        Peer peer = new Peer(9090, "./src/");
        Client cliente = new Client(peer);

    }
}