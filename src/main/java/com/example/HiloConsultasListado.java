package com.example;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class HiloConsultasListado implements Runnable{

    Socket client;
    Listado listado;

    public HiloConsultasListado(Socket client, Listado listado) {
        this.client = client;
        this.listado = listado;
    }

    @Override
    public void run() {
        try {
            DataInputStream flujo_entrada = new DataInputStream(client.getInputStream());
            String filename = flujo_entrada.readUTF();
            listado.buscar(filename);
            
            //envio respuesta

            ObjectOutputStream objeto_salida = new ObjectOutputStream(client.getOutputStream());
            objeto_salida.writeObject(listado.getFiles());
            client.close();
        }catch (Exception e){
            e.printStackTrace();
        }        
    }

}
