package com.example;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ActualizadorListado implements Runnable {
    Listado listado;
    public ActualizadorListado(Listado listado) {
        this.listado=listado;
    }
    @Override
    public void run() {
        ServerSocket ss;
        try {
            //TODO implementar logs
            ss = new ServerSocket(9001);
            System.out.println("ActualizadorListado: Estoy escuchado en 9001");

        while (true){
            Socket client = ss.accept();
            
            System.out.println("ActualizadorListado: AConsultastendiendo al cliente: "+client.getPort());

            HiloActualizadorListado sh = new HiloActualizadorListado(client, listado);
            // 2do paso
            Thread serverThread = new Thread(sh);
            // 3er paso
            serverThread.start();

        }
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }        
    }

}
